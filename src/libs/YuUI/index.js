import YUButton from "./components/YUButton";
import YUAlert from "./components/YUAlert";

const componentMap = [YUButton, YUAlert];
export default {
  install: (app, options) => {
    console.log(app, options);
    const { btnShadow } = options ? options : {};

    app.config.globalProperties.$YuUI = {
      btnShadow: !!btnShadow, // 按钮是否要有阴影 boolean值
    };

    // 全局注册组件
    componentMap.forEach((component) => {
      app.component(component.name, component);
    });
  },
};
