import { createApp } from "vue";
import App from "./App.vue";
import YuUI from "./libs/YuUI";

const app = createApp(App);
app.use(YuUI, {
  btnShadow: true,
});

app.mount("#app");
